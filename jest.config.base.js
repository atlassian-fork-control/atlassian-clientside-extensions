module.exports = {
    verbose: true,
    testTimeout: 15000,
    preset: 'ts-jest', // https://kulshekhar.github.io/ts-jest/user/config/
    modulePaths: ['<rootDir>/lib'],
    testPathIgnorePatterns: ['/node_modules/', '/dist/', '/target/', '/__fixtures__/'],
    collectCoverage: true,
    // this will make sure to collect coverage from all file, even if they don't have any test
    // https://jestjs.io/docs/en/configuration.html#collectcoveragefrom-array
    collectCoverageFrom: ['**/*.{ts,tsx}', '!**/__tests__/**', '!**/dist/**', '!**/node_modules/**'],
};
