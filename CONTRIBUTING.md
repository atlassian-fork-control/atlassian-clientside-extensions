# Contributing to Client-side Extensions

## Repository structure

This repository is a "monorepo".
It houses multiple artifacts for making the Client-side Extensions APIs work.
It contains both Maven and Node packages.
Each have their own workflow for building and releasing independently.

## Prerequisites

You will need the following tools in your development environment:

-   Maven 3.6.1
-   Java 8+

If you are working on the front-end code and not managing your workflow via Maven,
you will need additional tools in your development environment:

-   Node 8+
-   Yarn 1.7+

## Getting started

In the root folder, run `mvn clean install`. This will build all the projects and packages in the monorepo.

The following commands are available to you:

-   `mvn clean install` - Cleans, Installs and Builds the Demo, Runtime and Packages using Maven.
-   `yarn start:refapp` - Runs the Refapp demo project.
-   `yarn watch` - Provides watchmode and hot reload to Packages/Demo while running the Refapp.
-   `yarn lint` - Runs all code rules and reports violations.
-   `yarn lint --fix` - Runs all code rules and fixes any violations that can be fixed programmatically.
-   `yarn test` - Runs tests for all packages
-   `yarn test:watch` - Runs tests for all packages in watch mode

## Code rules

All commits will be checked for conformance to the code rules.

### Frontend code

All frontend code in this repository conforms to the [Server Frontend Platform team's code rules (pending)](https://hello.atlassian.net/wiki/spaces/TEC/pages/540067361/Code+formatting).

-   Code style is enforced through Prettier.
-   Code quality is enforced through ESLint.

### Backend code

All backend code must conform to [The Server Backend Platform team's code rules (pending)](https://hello.atlassian.net/wiki/spaces/AB/pages/331784335/Forming+the+team).

## Test configuration

### base

We group al common Jest configuration in `jest.config.base.js`. If you need to modify the configuration of ALL packages
(including root config), please apply you changes in this file

### root config

We mantain a root Jest configuration that make use of jest projects to ran all package tests in a single run
and generate a single `coverage` report. Try to avoid adding something here.

### packages config

Each package extends `base` configuration, and are free to add anything extra if they need to (ex: envs). Be wise
on editing these configs, and limit it to only modify per package use case. Otherwise, modify the `base` if the configuration
can be shared across packages.

## Testing local version in a Product

If you need to test your local changes in a product, you'll need to build the artifacts/packages you're editing from their sources, and add them to the product as follow:

### atlassian-clientside-extensions-runtime

1. Clone this repository from `master`
2. Compile Locally

    ```
    mvn clean install
    ```

3. Update your product to consume your local version of the runtime plugin. Or, you can also upload the `.jar` of your local version to the product via “Manage Apps”.

### @atlassian/clientside-extensions-\* packages

1.  Clone this repository from `master`
2.  Build Locally

    ```
    yarn build
    ```

3.  Link your local version of the library using either npm or yarn

    ```
    cd packages/* && yarn link
    ```

4.  Link your local version of the lib into your project using npm or yarn

    ```
    yarn link @atlassian/clientside-extensions-*
    ```

    This will be as if you were installing the package from npm, but you’re just creating a link in your project to go look for that package locally instead.

# Documentation

Documentation is written using [DAC writing toolkit](https://developer.atlassian.com/platform/writing-toolkit).

Make sure to got through the (Getting Started)[https://developer.atlassian.com/platform/writing-toolkit/getting-started/] guide to
gain access to the right package and docker images to work with the writing toolkit.

Refer to [documentation](/documentation) package for details about all the commands you can use while developing with the writing toolkit.

# Pipelines

Each push to a branch triggers a Bitbucket pipeline build to check if your code builds successfully. Our builds rely on a custom docker image (atlassian/server-client-plugins) on [Docker Hub](https://hub.docker.com/r/atlassian/server-client-plugins).

# Releasing

Each project in the monorepo is released independently and manually to [Atlassian's private registry](https://packages.atlassian.com).

So far, we've been keeping the versions of components + registry + P2 plugin in sync and release whatever changes we have in any of the pacakges at the same time under the same version.

Active development "starts" with pre-release version -alpha.0, meaning that each version release needs to be followed by version bump to next pre-release.

## Release Packages first

We use Lerna to manage the versions of our Packages

### Canary version

```bash
# release the canary version with this yarn command
$ release:packages:next
```

_Don't push your changes to git._

### Minor Version

```bash
# release the minor version with this yarn command
$ release:packages
```

## Then, release Runtime artifact

```bash
# prepare for release. Will guide you on setting a version, git tag and the next snapshot version.
$ mvn release:prepare

# publish the new version to atlassian registry. Will also push the changes to git.
$ mvn release:perform
```

For the SCM tag, we are using the format `v{version.number}`. E.g: `v0.2.1`.
