package com.atlassian.rest.plugins;

import com.atlassian.plugin.web.api.WebItem;
import com.google.common.collect.Lists;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.junit.MockitoJUnit;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ClientsideExtensionsAssetsBeanTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    private ClientsideExtensionsAssetsBean bean;

    @Test
    public void testAssetsBeanGetWebItemsReturnsListOfWebItemsBean() {
        // Arrange
        WebItem webItem = mock(WebItem.class);
        // Act
        bean = new ClientsideExtensionsAssetsBean(singletonList(webItem));
        // Assert
        assertThat(bean.getWebItems().size(), is(1));
        assertThat(bean.getWebItems(), instanceOf(List.class));
        assertThat(bean.getWebItems().get(0), instanceOf(WebItemBean.class));
    }

    @Test
    public void testAssetsBeanSortedWeights() {
        // Arrange
        List<WebItem> webItems = Lists.newArrayList();
        WebItem webItemOne = mock(WebItem.class);
        WebItem webItemTwo = mock(WebItem.class);
        when(webItemOne.getWeight()).thenReturn(10);
        when(webItemTwo.getWeight()).thenReturn(999);
        webItems.add(webItemTwo);
        webItems.add(webItemOne);
        bean = new ClientsideExtensionsAssetsBean(webItems);
        // Act
        List<WebItemBean> clientWebItems = bean.getWebItems();
        // Assert
        assertThat(clientWebItems.get(0).getWeight(), is(10));
        assertThat(clientWebItems.get(1).getWeight(), is(999));
    }
}
