# Client-side Extensions

## Provider Components

React components to assist in the authoring of plugin points that work at product runtime.

Use of these components is optional, but makes rendering client-side extensions much easier,
since it provides default handlers for each plugin type that already implement Atlaskit.

Refer to the [official documentation](https://developer.atlassian.com/server/framework/clientside-extensions) for more information.
