const observeStateChange = jest.fn();
const isDebugEnabled = jest.fn();
const isLoggingEnabled = jest.fn();
const isValidationEnabled = jest.fn();
const isDiscoveryEnabled = jest.fn();

export { observeStateChange, isDebugEnabled, isLoggingEnabled, isValidationEnabled, isDiscoveryEnabled };
