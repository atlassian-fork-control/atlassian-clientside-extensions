import { LoggerPayload, LogLevel, observeLogger } from '../debug';

const getLoggingFromLevel = (level: LogLevel): keyof typeof console => {
    switch (level) {
        case LogLevel.error:
            return 'error';
        case LogLevel.debug:
            return 'debug';
        case LogLevel.info:
            return 'info';
        case LogLevel.trace:
            return 'trace';
        case LogLevel.warn:
            return 'warn';
        default:
            throw new Error(`Unexpected LogLevel: "${level}"`);
    }
};

const componentsToReadable = (components?: string | string[]) => {
    if (!components) {
        return '';
    }

    const componentsArray = Array.isArray(components) ? components : components.split('.');

    return `Component: ${componentsArray.join(' → ')}`;
};

const metaFormatter = (_: unknown, value: unknown): string => {
    if (value === null) {
        return 'null';
    }

    if (value === undefined) {
        return 'undefined';
    }

    if (Array.isArray(value)) {
        return value.join(', ');
    }

    if (typeof value === 'function') {
        return value.toString();
    }

    return value as string;
};
const metaValueToReadable = (value: unknown) => {
    return JSON.stringify(value, metaFormatter)
        .replace(/^"|"$/g, '')
        .replace(/"/g, ' ');
};
const metaToReadable = (meta?: LoggerPayload['meta']) => {
    if (!meta) {
        return '';
    }
    const separator = '\n\t\t';

    return `Meta information:${separator}${Object.keys(meta)
        .map(key => `${key}: ${metaValueToReadable(meta[key])}`)
        .join('\n\t\t')}`;
};

export default () => {
    observeLogger(payload => {
        const consoleKey = getLoggingFromLevel(payload.level);
        // eslint-disable-next-line no-console
        console[consoleKey](`[Atlassian Client-side Extensions]
${componentsToReadable(payload.components)}

${payload.message}

${metaToReadable(payload.meta)}
`);
    });
};
