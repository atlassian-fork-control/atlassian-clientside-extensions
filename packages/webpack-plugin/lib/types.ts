import { TransformedComment } from './comment-transformer';

export interface ClientsideExtensionsManifest {
    filePath: string;
    id: string;
    key: string;
    section: string;
    condition?: TransformedComment['condition'];
    label?: string;
    link?: string;
    weight?: number;
}

export interface ClientsideExtensionsOptions extends ClientsideExtensionsManifest {
    entryPoint: string;
    fullyQualifiedNamespace: string;
    pluginKey: string;
}

export interface IWrmPluginOptions {
    assetContentTypes: string;
    conditionMap: object;
    contextMap: object;
    locationPrefix: string;
    noWRM: boolean;
    pluginKey: string;
    providedDependencies: object;
    transformationMap: object;
    verbose: boolean;
    watch: boolean;
    watchPrepare: boolean;
    webresourceKeyMap: object;
    xmlDescriptors: string;
}
