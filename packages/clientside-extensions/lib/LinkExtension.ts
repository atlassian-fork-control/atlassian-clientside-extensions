import { PluginAttributesProvider } from '@atlassian/clientside-extensions-registry';

// eslint-disable-next-line import/prefer-default-export
export const factory = (provider: PluginAttributesProvider): PluginAttributesProvider => {
    return (...args) => {
        return {
            ...provider(...args),
            type: 'link',
        };
    };
};
