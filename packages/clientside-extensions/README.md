# Client-side Extensions

## Extension Factories

Collection of function factories to simplify the consumption of extension points.

Refer to the [official documentation](https://developer.atlassian.com/server/framework/clientside-extensions) for more information.
