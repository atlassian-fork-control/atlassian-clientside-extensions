const getLocationPlugins = jest.fn();
const getLocationResources = jest.fn();

export { getLocationPlugins, getLocationResources };
