---
title: CSE webpack plugin - Client-side Extensions
platform: server
product: clientside-extensions
category: reference
subcategory: webpack
date: '2020-02-25'
---

# Annotations

The provided webpack plugin uses annotations in comments to gather information about the plugin, and generate the XML for you.

## Supported annotations

<table>
    <colgroup>
        <col width="20%" />
        <col width="10%" />
        <col width="70%" />
    </colgroup>
    <thead>
        <tr class="header">
            <th>Name</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>@clientside-extension*</td>
            <td><code>-</code></td>
            <td>Indicates that the next function is a plugin factory to be consumed by the webpack plugin.</td>
        </tr>
        <tr>
            <td>@extension-point*</td>
            <td><code>string</code></td>
            <td>Defines the location where the plugin will be rendered.</td>
        </tr>
        <tr>
            <td>@weight</td>
            <td><code>number</code></td>
            <td>
                <p>Determines the order in which this plugin appears respect to others in the same location.</p>
                <p>Plugins are displayed top to bottom or left to right in order of ascending weight.</p>
            </td>
        </tr>
        <tr>
            <td>@condition</td>
            <td><code>string | Conditions</code></td>
            <td>
                <p>Defines one or multiple conditions that must be satisfied for the client plugin to be displayed.</p>
                <p>This conditions are just web-fragment conditions evaluated in the server, and created with Java.</p>
                <p>If one of the conditions is not met, the code of the plugin won't be loaded in the client.</p>
            </td>
        </tr>
    </tbody>
</table>

<p><strong>* required</strong></p>

## Usage notes

### 1. Define a client plugin and the location to be rendered in

```ts
import { LinkPlugin } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point reff.plugins-example-location
 */
export default LinkPlugin.pluginFactory(/*...*/);
```

### 2. Specify a weight

```ts
import { LinkPlugin } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point reff.plugins-example-location
 * @weight 100
 */
export default LinkPlugin.pluginFactory(/*...*/);
```

### 3. Define a condition

```ts
import { LinkPlugin } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point reff.plugins-example-location
 * @condition class com.atlassian.clientpluginsdemorefapp.RandomCondition
 */
export default LinkPlugin.pluginFactory(/*...*/);
```

### 4. Defining multiple conditions

You can make use of AND / OR keywords to combine multiple conditions for your client plugin as follow:

```ts
import { LinkPlugin } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point reff.plugins-example-location
 * @condition type AND
 * @condition conditions.0.type OR
 * @condition conditions.0.conditions.0.class class com.atlassian.clientpluginsdemorefapp.RandomCondition
 * @condition conditions.0.conditions.0.invert true
 * @condition conditions.0.conditions.0.params.attributes.name permission
 * @condition conditions.0.conditions.0.params.value admin
 * @condition conditions.0.conditions.1.class class com.atlassian.clientpluginsdemorefapp.SuperCondition
 * @condition conditions.0.conditions.1.invert true
 * @condition conditions.0.conditions.1.params.attributes.name permission
 * @condition conditions.0.conditions.1.params.value project
 * @condition conditions.1.class class com.atlassian.clientpluginsdemorefapp.PerfecCondition
 * @condition conditions.1.params.attributes.name permission
 * @condition conditions.1.params.value admin
 */
export default LinkPlugin.pluginFactory(/*...*/);
```
