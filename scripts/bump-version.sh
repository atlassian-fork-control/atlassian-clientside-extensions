version="${1}"

# bump FE packages version. Avoiding tags/git-commit so that we can have a single commit with mvn/npm changes
npm run lerna version "${version}" -- --no-git-tag-version --force-publish --no-push --yes

# bump runtime version
mvn versions:set --batch-mode -DnewVersion="${version}" -DgenerateBackupPoms=false
