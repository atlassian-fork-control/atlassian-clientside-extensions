# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

### Changed

-   All methods of the `useExtensions` are now optionally generic and accept the shape of `attributes` the developer expects to receive.

## [1.0.0]

First stable release of Client-side Extensions 🎉.

No changes were made from 1.0.0-rc-1.

## [1.0.0-rc-1]

These are the last set of breaking changes before our 1.0 release.

### Changed

-   **BREAKING CHANGE** `ExtensionPointInfo` now takes the schemas as a prop instead of getting them from the registry.

### Removed

-   **BREAKING CHANGE** Registry no longers saves a copy of extension schemas. If you're upgrading your registry version, you must also use the new version of ExtensionPointInfo.
-   **BREAKING CHANGE** Removed deprecated Modal options. You should now use the same methods provided in the ModalAPI instead.

## [0.9.0]

### Added

-   `setActions` and `onClose` methods has been added to the top level ModalAPI. They will replace the ones provided as "options" in onMount modal method.
-   `setWidth` and `setAppearance` methods have been added to ModalAPI.
-   `setAction` ModalAPI now can an accept a new `testId` property. You can use this property to write unique selectors while working on the e2e tests.

### Fixed

-   Runtime no longer bundles React/AJV.
-   The existing JSON schema fields like `label`, `url`, `title` are not getting overwritten with default values.
-   Attributes are now validated also when calling `updateAttributes`.

### Changed

-   **BREAKING CHANGE**: The `useDebug`, `useValidation`, `useLogging`, `useDiscovery` React hooks were removed from the
    `@atlassian/clientside-extensions-debug` package and moved to the `@atlassian/clientside-extensions-components` package.
-   Runtime no longer validates the type of extensions in production mode.

### Deprecated

-   `setActions` and `onClose` Modal Options API is deprecated in favor for their counterparts in ModalAPI. They are still available
    for you to use, but will be removed on a later version.

## [0.8.1]

### Fixed

-   Releasing packages without `dist` folders. There was a problem with our pipelines, so 0.8.0 got released incorrectly.

## [0.8.0]

### Added

-   Error boundaries to protect products from crashing when something goes wrong with extensions.

### Changed

-   Changed `registerPlugin` registry API to `registerExtension`. The webpack plugin was updated too, so re-compile your extensions after upgrading.

## [0.7.0]

-   Rename the Java runtime package `com.atlassian.plugins.client-plugins-runtime` → `com.atlassian.plugins.atlassian-clientside-extensions-runtime`
-   Canonical repository location changed to `https://bitbucket.org/atlassian/atlassian-clientside-extensions`

## [0.6.0]

This release is focused on changing the name of our APIs and packages and public release.

### Changed

#### Packages

-   All packages changed their name from `@atlassian/client-plugins-*` to `@atlassian/clientside-extensions-*`.
-   Registry AMD module is now called `@atlassian/clientside-extensions-registry`. Use this name when configuring the provided
    dependencies while using WRM webpack plugin.

#### Client-side API

-   `LinkPlugin` → `LinkExtension`
-   `ButtonPlugin` → `ButtonExtension`
-   `PanelPlugin` → `PanelExtension`
-   `ModalPlugin` → `ModalExtension`
-   Method to create a factory is also renamed for all extensions: `.pluginFactory` → `.factory`

#### Client-side components API

-   `usePluginsAll` → `useExtensionsAll`
-   `usePlugins` → `useExtensions`
-   `usePluginsUnsupported` → `useExtensionsUnsupported`
-   `usePluginsLoadingState` → `useExtensionsLoadingState`
-   `PluginLocation` → `ExtensionPoint`
-   `PluginLocationInfo` → `ExtensionPointInfo`
-   `ModalRenderPlugin` → `ModalRenderExtension`
-   `PanelRenderPlugin` → `PanelRenderExtension`

#### Client-side webpack plugin annotations

-   `@client-plugin` → `@clientside-extension`
-   `@location` → `@extension-point`

### Fixed

-   Removed `jest` from registry peer dependencies.
-   Validation now is activated on `validation` instead of `debug` flag is on.

## [0.5.0]

### Added

-   Debug flags to activate Debugging, Logging, Validation and Discovery of locations.
-   Hooks to get/set new debug Flags.
-   `__withoutTransition` option for ModalRenderer to get a Modal component without Transition. Usefull when you need to control the container of the Modal.
    (e.j: rendering a modal when a dropdown item is clicked).

### Changed

-   Validation now uses AJV to validate schemas. This mean we fully support JSON Schemas.
-   Modals' `setAction` now takes an array of actions that allows to define the click handler, loading and disabled state.

### Removed

-   `?client-plugins` query param is removed from the plugin system. Products should now provide this functionality with their
    routing solutions using the provided hooks to set the discovery flag.
-   Modal's `onAction`, `onPrimaryAction` and `onSecondaryAction` have been removed in favor of new `setActions` API,

## [0.4.1]

### Changed

-   Fix wrong export of `onDebug` utility.

## [0.4.0]

### Added

-   Validation for Attribute Schemas.
-   New hooks `usePlugins`, `usePluginsAll`, `usePluginsLoadingState`, `usePluginsUnsupported`.
-   Added `onModalAction` to ModalAPI. Useful when a modal has more than two actions.
-   Webpack plugin now supports setting server `conditions`.
-   Added `onCleanup` cycle to PluginAPI that will be executed when destroying the plugin

### Changed

-   Since Client-side Extensions no longer depends on `entry-point` element for `web-items`, the minimal version of Webfragments artifact
    has been changed to `5.0` instead of `5.1`. Entrypoint element will be deleted in a later Webfragments version.
-   Plugin Factory is now a single function that takes both the `pluginAPI` and `context`, instead of being a closure. Check
    the documentation to familiarise yourself with the new signature.
-   `requestRender` API has been changed to `updateAttributes` API, and now it takes the set of attributes you want to change
    instead of triggering an execution of your attributes provider. Check the documentation to familiarise yourself with the new signature.
-   `usePlugins` has been renamed to `usePluginsAll`. A new hook `usePlugins` now returns only the list of plugins with a type
    that the location supports.

## [0.3.0]

### Added

-   Introduced Plugin types.
-   Introduced ClientPlugins Webpack plugin.
-   New Documentation to include Plugin types examples.
-   Chore: Added watchmode to all our packages and demo.
-   Chore: Added bitbucket pipelines to check our build/lint before merging.

### Changed

-   Plugin resources are loaded by Context instead of Resource key.
-   `registry.registerPlugin` now takes Attribute Factories instead of `ClientPlugins` classes. If you need to
    create an equivalent of a WebPanel, use a Panel plugin instead.
-   `PluginPointInfo` component is renamed to `PluginLocationInfo`.
-   The query param to activate the PluginLocationInfo changed from `?plugin-points=true` to `client-plugins-true`.
-   Major refactore of our code base to remove any reference to WebItem/WebPanel. Now everything is a Plugin.

### Removed

-   `<web-entry-point>` custom module was removed. Use a regular `<web-resource>` instead.
-   `registry.registerWebItemCallback` method was removed, and you should now use `registry.registerPlugin` instead.

### Deprecated

-   `<entry-point>` element for WebItemModule is no longer supported, and will be removed in a future version of [Webfragments](https://bitbucket.org/atlassian/atlassian-plugins-webfragment/src)
