import { ButtonExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point reff.plugins-example-location
 */
export default ButtonExtension.factory(api => {
    return {
        label: 'Click and I 💣💥',
        onAction() {
            api.updateAttributes(() => {
                throw new Error('💣💥');
            });
        },
    };
});
