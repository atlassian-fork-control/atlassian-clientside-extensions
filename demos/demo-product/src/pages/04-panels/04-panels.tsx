import React from 'react';
import { PluginDescriptor } from '@atlassian/clientside-extensions-registry';
import { PanelHandler, useExtensions } from '@atlassian/clientside-extensions-components';
import { PanelExtension, renderElementAsReact } from '@atlassian/clientside-extensions';
import { ExtensionPointDataDemo04 } from './types';
import { schema, contextSchema } from './schema';

const asPanels = (ext: PluginDescriptor) => {
    const { label, onAction } = ext.attributes;
    return (
        <React.Fragment key={ext.key}>
            <h3>{label}</h3>
            <PanelHandler.PanelRenderer render={onAction as PanelHandler.PanelRenderExtension} />
        </React.Fragment>
    );
};

const extensionData: ExtensionPointDataDemo04 = {
    title: `Dragon's rest`,
};

const PanelsDemo = () => {
    const descriptors = useExtensions('demo.04-panels', extensionData, { schema, contextSchema });

    return (
        <>
            <h1>Panel plugins</h1>
            <p>
                Product developers can allow plugin vendors to provide their own rendering logic via a <dfn>panel</dfn> plugin type.
            </p>

            {descriptors.map(asPanels)}
        </>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, PanelsDemo);
};
