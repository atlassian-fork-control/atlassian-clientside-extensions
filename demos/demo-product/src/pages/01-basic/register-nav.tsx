import { AsyncPanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point demo-product.extend-navigation
 */
export default AsyncPanelExtension.factory(() => {
    return {
        label: 'Basic demo',
        section: 'basic.section',
        onAction() {
            return import(/* webpackChunkName: "demo-basic" */ './01-basic');
        },
    };
});
