import React, { FC } from 'react';
import { PluginDescriptor } from '@atlassian/clientside-extensions-registry';
import { PanelHandler, useExtensionsAll } from '@atlassian/clientside-extensions-components';
import Tabs from '@atlaskit/tabs';
import { TabData } from '@atlaskit/tabs/types';
import { schema, contextSchema } from './schema';
import { ExtensionPointDataDemo05 } from './types';

const asTabParams = (ext: PluginDescriptor): TabData => {
    const id = ext.key;
    const { label, onAction, isActive } = ext.attributes;

    return {
        label: `${label}`,
        key: id,
        'aria-selected': isActive,
        content: <PanelHandler.PanelRenderer render={onAction as PanelHandler.PanelRenderExtension} />,
    };
};

const AKTabsDemo: FC<{ data: ExtensionPointDataDemo05 }> = ({ data }) => {
    const [descriptors, , isLoading] = useExtensionsAll('demo.05-tabs', data, { schema, contextSchema });

    // Initialise tabs once all plugins have loaded.
    if (!isLoading && descriptors.length) {
        // select the first tab by default.
        descriptors[0].attributes.isActive = true;
    }

    return (
        <section className="aui-tabs horizontal-tabs">
            <Tabs tabs={descriptors.map(asTabParams)} />
        </section>
    );
};

export default AKTabsDemo;
