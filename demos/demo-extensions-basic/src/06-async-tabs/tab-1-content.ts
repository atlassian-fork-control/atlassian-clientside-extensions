import { PanelExtension } from '@atlassian/clientside-extensions';

interface data {
    greetings: string[];
}

export default (panelApi: PanelExtension.Api, data: data) => {
    const anotherGreeting = () => {
        const idx = Math.floor(Math.random() * data.greetings.length);
        const greeting = data.greetings[idx];
        return `<p>${greeting}</p>`;
    };

    panelApi.onMount(element => {
        element.innerHTML += anotherGreeting();
    });
};
