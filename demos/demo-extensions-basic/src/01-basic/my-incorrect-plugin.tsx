import { ButtonExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point demo.01-basic
 */
export default ButtonExtension.factory(() => {
    return {
        // This button should have a "label", but the plugin author messed up the spelling.
        // Depending on how the plugin point schema was configured by the product developers,
        // one of two things will happen:
        // - If the attribute was "required", the plugin will not be rendered at all.
        // - Otherwise, the plugin will be rendered. Some weird things might happen if the product developers
        //   didn't bulletproof their code.
        laaaabel: 'I sure hope this renders!',
    };
});
