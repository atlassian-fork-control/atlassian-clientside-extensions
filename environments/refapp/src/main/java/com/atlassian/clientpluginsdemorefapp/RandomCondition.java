package com.atlassian.clientpluginsdemorefapp;

import com.atlassian.plugin.web.Condition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class RandomCondition implements Condition {

    private static final Logger log = LoggerFactory.getLogger(RandomCondition.class);

    @Override
    public void init(Map<String, String> map) {

    }

    @Override
    public boolean shouldDisplay(Map<String, Object> map) {
        // this is being called multiple times - and should ideally return the same always. Which cant be guaranteed but is pretty close...
        final long currentSeconds = System.currentTimeMillis() / 1000;
        final boolean shouldDisplay = currentSeconds % 2 == 0;
        log.warn("The current second is: {}, 'shouldDisplay' is therefore {}", currentSeconds, shouldDisplay);
        return shouldDisplay;
    }
}
