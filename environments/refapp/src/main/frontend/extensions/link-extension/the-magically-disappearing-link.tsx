import { LinkExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point reff.plugins-example-location
 * @condition class com.atlassian.clientpluginsdemorefapp.RandomCondition
 */
export default LinkExtension.factory(() => {
    return {
        type: 'link',
        label: 'I have a random condition. Click me and I might disappear. Who knows? 🤷',
        url: window.location.href,
    };
});
